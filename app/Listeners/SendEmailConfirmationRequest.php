<?php

namespace App\Listeners;

use App\Events\PhotographerRegistered;
use App\Mail\PleaseConfirmYourEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailConfirmationRequest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PhotographerRegistered  $event
     * @return void
     */
    public function handle(PhotographerRegistered $event)
    {
        //
        Mail::to($event->photographer)->send(new PleaseConfirmYourEmail($event->photographer));
    }
}
