<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lens extends Model
{
    public $table = 'lens';
}
