<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotographerCategory extends Model
{
    protected $table = 'photographer_category';

    protected $fillable = ['category_id', 'photographer_id'];

    public function category()
    {
        return $this->belongsTo(PhotographyCategory::class, 'category_id');
    }
}
