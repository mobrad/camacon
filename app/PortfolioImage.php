<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PortfolioImage extends Model
{
    public $fillable = ['photographer_id', 'photography_category_id'];

    protected $hidden = ['photography_category_id'];

    /**
     * Gets the Photography category the Porfolio Image belongs to.
     *
     **/
    public function category()
    {
        return $this->belongsTo('App\PhotographerCategory', 'photography_category_id');
    }

    /**
     * Gets all the likes for the Portfolio Image.
     *
     * @return Collection
     **/
    public function likes()
    {
        return $this->hasMany(Like::class, 'image_id');
    }

    /**
     * Gets the like for the Portfolio Image.
     *
     * @return string
     **/
    public function url_link()
    {
        return Storage::disk('photographer_images')->url($this->name);
    }
}
