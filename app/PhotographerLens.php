<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotographerLens extends Model
{
    //
    protected $table = 'photographer_lens';
}
