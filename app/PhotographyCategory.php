<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotographyCategory extends Model
{
    public function images()
    {
        return $this->hasMany(PortfolioImage::class, 'photography_category_id');
    }
    public function getRouteKeyName()
    {
        return 'name';
    }
}
