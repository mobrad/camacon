<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Photographer extends Authenticatable
{
    use HasApiTokens;

    public $primaryKey = 'id';

    protected $guarded = [];

    public $fillable = ['id', 'full_name', 'city', 'shoot_hour', 'studio_name', 'address', 'email', 'password'];

    protected $casts = [
        'confirmed' => 'boolean',
    ];

    public function confirm()
    {
        $this->confirmed = true;
        $this->confirmation_token = null;
        $this->save();
    }

    public function rating()
    {
        $rating = Rating::where('photographer_id', $this->id)->get()
            ->pluck('rating')->avg();

        return round($rating, 1);
    }

    public function equipments()
    {
        return [
            'cameras' => $this->cameras,
            'lens' => $this->lenses,
        ];
    }

    public function cameras()
    {
        return $this->belongsToMany('App\Camera', 'photographer_camera', 'photographer_id', 'camera_id');
    }

    public function lenses()
    {
        return $this->belongsToMany('App\Lens', 'photographer_lens', 'photographer_id', 'lens_id');
    }

    public function images()
    {
        return $this->hasMany('App\PortfolioImage', 'photographer_id');
    }

    public function category()
    {
        return $this->belongsToMany(
            'App\PhotographyCategory',
            'photographer_category',
            'photographer_id',
            'category_id'
        );
    }

    public function path()
    {
        return route('portfolio', $this->id);
    }

    // public function category_image($id){
    //     //We want to get all the images in the photographer's category
    //     //
    //     photographer->category($id)->image;
    // }
}
