<?php

namespace App\Http\Controllers;

use App\Photographer;

class PhotographerController extends Controller
{
    public function create()
    {
        return view('register');
    }

    public function index()
    {
        $photographers = Photographer::all();

        return view('photographers', compact('photographers'));
    }

    public function store()
    {
        request()->validate([
            'fullName' => 'required',
            'studioName' => 'required',
            'phoneNumber' => 'required|min:10',
            'email' => 'required|string|email|max:255|unique:photographers',
            'city' => 'required',
            'address' => 'required',
            'password' => 'required|min:8',
            'shootHour' => 'required|numeric',
        ]);

        $photographer = Photographer::create([
            'full_name' => request('fullName'),
            'studio_name' => request('studioName'),
            'tel' => request('phoneNumber'),
            'email' => request('email'),
            'city' => request('city'),
            'address' => request('address'),
            'password' => bcrypt(request('password')),
            'shoot_hour' => request('shootHour'),
            'confirmation_token' => str_limit(md5(request('email').str_random()), 25, ''),
        ]);

        return json_encode($photographer);
    }

    public function complete()
    {
        return view('registration_complete');
    }

    public function update($id)
    {
        request()->validate([
            'fullName' => 'required',
            'studioName' => 'required',
            'phoneNumber' => 'required|min:10',
            'email' => 'required|string|email|max:255',
            'city' => 'required',
            'address' => 'required',
            'password' => 'required|min:8',
            'shootHour' => 'required|numeric',
        ]);

        Photographer::find($id)->update([
            'full_name' => request('fullName'),
            'studio_name' => request('studioName'),
            'tel' => request('phoneNumber'),
            'email' => request('email'),
            'city' => request('city'),
            'address' => request('address'),
            'password' => bcrypt(request('password')),
            'shoot_hour' => request('shootHour'),
        ]);
    }

    public function getEquipments(Photographer $photographer)
    {
        return [
            'cameras' => $photographer->cameras->toArray(),
            'lenses' => $photographer->lenses->toArray(),
        ];
    }
}
