<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:web');
    }

    /**
     * Authenticated users likes an image.
     *
     * @todo Make a test against multiple image likes.
     *
     * @param int $image_id - the liked image id
     *
     * @return Json
     **/
    public function store($image_id)
    {
        if (!auth('api')->check()) {
            return abort(401, 'Unauthorized action.');
        }

        $params = [
            'user_id' => auth('api')->id(),
            'image_id' => $image_id,
        ];

        $like = Like::where($params)->get();
        (bool) $like->count() ? $like->first()->delete() : Like::create($params);

        return ['status' => !(bool) $like->count()];
    }
}
