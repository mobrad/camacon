<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
 use Illuminate\Foundation\Auth\AuthenticatesUsers;
 use Illuminate\Validation\ValidationException;
 use Auth;

class PhotographerLoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Photographer Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating photographers for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
   
    public function login(){
    	$validator = request()->validate([
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);

        //attempt to login
        if(Auth::guard('photographer')->attempt(['email'=> request('email'),'password' => request('password')])){
            return Auth::guard('photographer')->user()->id;
        }

        //if successfull the redirect to their intended target

        //if unsuccessful then redirect to the login with the form data
        $this->sendLoginErrorResponse();
    }
    
    protected function sendLoginErrorResponse(){
    	throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }
    public function username()
    {
        return 'email';
    }
}
