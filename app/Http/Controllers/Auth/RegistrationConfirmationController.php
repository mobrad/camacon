<?php
/**
 * Created by PhpStorm.
 * User: Tammy
 * Date: 05/06/2018
 * Time: 10:48
 */

namespace App\Http\Controllers\Auth;


use App\Photographer;

class RegistrationConfirmationController
{
    public function photographer(){
        $photographer = Photographer::where('confirmation_token',request('token'))->first();
        if(! $photographer) {
            return redirect(route('home'))->with('flash','invalid token');
        }
        $photographer->confirm();
        return redirect(route('create.password'))->with('flash','Your account has been confirmed');
    }
}