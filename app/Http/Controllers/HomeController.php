<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    public function index()
    {
        if (auth('photographer')->check()) {
            return redirect('/dashboard');
        } elseif (auth()->check()) {
            return redirect()->route('photographer-finder');
        } else {
            return redirect('/');
        }
    }
}
