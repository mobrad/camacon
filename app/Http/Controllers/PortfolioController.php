<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PhotographerCategory;
use App\Portfolio;

class PortfolioController extends Controller
{
    //
    public function store(){
    	request()->validate([
    		'portfolioLink' => 'required',
    		'instagramHandle' => 'required',
    		'facebookPage' => 'required',
    		'twitterHandle' => 'required',
            'categories_id' => 'required'
    	]);
        //Create the categories the photographer belongs to
        $this->createPhotographerCategory();
        Portfolio::forceCreate([
            'photographer_id' => request('id'),
            'portfolio_link' =>  request('portfolioLink'),
            'instagram_handle' => request('instagramHandle'),
            'facebook_page' => request('facebookPage'),
            'twitter_handle' => request('instagramHandle')
        ]);
        // Foreach category requested create a new photographers category
    }
    private function createPhotographerCategory(){
        $categories_id = request('categories_id');
        $categories_id = preg_replace('/\.$/', '', $categories_id); //Remove dot at end if exists
        $array = explode(',', $categories_id); //split string into array seperated by ', '
        foreach($array as $value) //loop over categories array
        {
            PhotographerCategory::forceCreate([
                'photographer_id' => request('id'),
                'category_id' => $value
            ]);
        }
    }

}
