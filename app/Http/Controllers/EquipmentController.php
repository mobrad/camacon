<?php

namespace App\Http\Controllers;

use App\Lens;
use App\Camera;
use App\Photographer;
use App\PhotographerLens;
use App\PhotographerCamera;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class EquipmentController extends Controller
{
    /**
     * Stores the photographers cameras in a database.
     *
     *
     * @return Json
     **/
    public function store()
    {
        extract(request()->all());
        //Make sure the fields are not Empty before
        request()->validate([
            'cameraValue' => 'array|required',
            'lensValue' => 'array|required',
        ]);

        $lensValue = Collection::make($lensValue);
        $cameraValue = Collection::make($cameraValue);
        $equipments = $cameraValue->merge($lensValue);

        $this->deleteDisownedEquipments($equipments)
            ->addNewEquipments($cameraValue, 'cameras')
            ->addNewEquipments($lensValue, 'lenses');

        return ['success' => 'Equipments Created'];
    }

    /**
     * Adds all new the cameras to the photographer camera database.
     *
     * @param Collection $cams - all cameras
     *
     * @return $this
     **/
    protected function addNewEquipments(Collection $allEquipments, $type)
    {
        $methods = 'lenses' === $type ? 'createLens' : 'createCamera';
        $oldEquipments = Photographer::find(request()->id)->{$type}
            ->pluck('id')->toArray();
        //filters the new equipments from my old equipments
        $allEquipments->filter(function ($equip) use ($oldEquipments) {
            return !in_array($equip['id'], $oldEquipments);
        })->map([$this, $methods]); //create equipment

        return $this;
    }

    /**
     * Deletes all the cameras that were removed by a photographer.
     *
     * @param Collection $cameraValue - the cameras selected
     *
     * @todo  update the PhotographerCamera migration adding a primaryKey
     *
     * @return $this
     **/
    public function deleteDisownedEquipments(Collection $cameraValue)
    {
        $cameras_id = $cameraValue->filter(function ($equipment) {
            return (bool) @$equipment['pivot']['camera_id'];
        });

        $lens_id = $cameraValue->filter(function ($equipment) {
            return (bool) @$equipment['pivot']['lens_id'];
        });

        $this->deleteEquipment($lens_id, 'lens');
        $this->deleteEquipment($cameras_id, 'camera');

        return $this;
    }

    public function getEquipmentModel($equipment_type = false)
    {
        return 'camera' === $equipment_type
            ? new PhotographerCamera()
            : new PhotographerLens();
    }

    public function deleteEquipment(Collection $equipments, string $equipment_type)
    {
        $model = $this->getEquipmentModel($equipment_type);
        $equipments_id = $equipments->pluck('id')->toArray();
        $columnName = $equipment_type.'_id';

        return $model::where(['photographer_id' => request()->id])
            ->whereNotIn($columnName, $equipments_id)->get()
            ->each(function ($equip) use ($columnName, $model) {
                DB::table($model->getTable())->where([
                    'photographer_id' => request()->id,
                    $columnName => $equip->{$columnName},
                ])->delete();
            });
    }

    // Create the camera on the fly, immediately request is received
    public function createCamera($new_camera)
    {
        PhotographerCamera::forceCreate([
            'photographer_id' => request('id'),
            'camera_id' => $new_camera['id'],
        ]);
    }

    // Create the camera on the fly, immediately request is received
    public function createLens($lens)
    {
        PhotographerLens::forceCreate([
            'photographer_id' => request('id'),
            'lens_id' => $lens['id'],
        ]);
    }
}
