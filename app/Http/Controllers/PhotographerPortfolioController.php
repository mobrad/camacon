<?php

namespace App\Http\Controllers;

use App\Photographer;

class PhotographerPortfolioController extends Controller
{
    public function show($id = false)
    {
        $id = $id ? $id : request()->user()->id;
        $photographer = Photographer::findOrFail($id);

        return view('portfolio', compact('photographer'));
    }
}
