<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PhotographerAvatar;

class PhotographerAvatarController extends Controller
{
    //
    public function store(){
    	//Creating photographer Avatar on the PhotographerAvatar
    	$photographerAvatar = PhotographerAvatar::forceCreate([
    		'photographer_id' => 1,
    		'avatar_path' => request()->file('avatar')->store('avatar','public')
    	]);
    	return response(['message' => 'upload successful'],200);
    }
}
