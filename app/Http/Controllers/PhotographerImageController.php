<?php

namespace App\Http\Controllers;

use App\Like;
use App\Photographer;
use App\PortfolioImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\PortfolioImages;

class PhotographerImageController extends Controller
{
    /**
     * Gets the images for a user by the photographer categories.
     *
     * @param int $id photogrpahers id
     *
     * @author Joseph Owonvwon
     *
     * @return Json
     **/
    public function getImages(Photographer $photographer)
    {
        $categories = $photographer->category;
        $cat_ids = $categories->pluck(['id'])->toArray();

        //gets the photographers images
        $images = PortfolioImage::where('photographer_id', $photographer->id)
            ->whereIn('photography_category_id', $cat_ids)
            ->with('likes')->paginate(10);

        return PortfolioImages::collection($images);
    }

    /**
     * Gets the like data for a particular image.
     * and so checkes if the user liked an image previously.
     *
     * @param int $image_id - the image id
     * @param int $user_id  - the users id
     *
     * @return Json
     *
     * @author Joseph Owonvwon
     **/
    public function getImageLikes($image_id, $user_id = null)
    {
        return (bool) $user_id ? $this->getLikeStatus($image_id, $user_id) : $this->getAllLikes($image_id);
    }

    /**
     * Gets all the like status for a particular Image.
     *
     * @param int $image_id - The image id for checking
     *
     * @return Json
     *
     * @author Joseph Owonvwon
     **/
    protected function getAllLikes($image_id)
    {
        return [
            'likes_count' => DB::table('likes')->where(compact('image_id'))->count(),
            'image_refd' => $image_id,
        ];
    }

    /**
     * Checks if an image was liked by a User and returns status.
     *
     * @param int $image_id - The image id for checking
     * @param int $user_id  - The User Id for checking
     *
     * @return Json Boolean
     *
     * @author Joseph Owonvwon
     **/
    protected function getLikeStatus($image_id, $user_id)
    {
        $status = (bool) DB::table('likes')->where(compact('image_id', 'user_id'))->count();

        return compact('status');
    }

    public function store()
    {
        foreach (request()->file('file') as $image) {
            $portfolioImage = PortfolioImage::forceCreate([
                'photography_category_id' => request('id'),
                'name' => $image->store('images', 'photographer_images'),
            ]);
            $photographerImage = PhotographerImage::forceCreate([
                'photographer_id' => request('photographer_id'),
                'image_id' => $portfolioImage->id,
            ]);
        }
    }

    public function createImages()
    {
        //for each request image as image get the name and the category and story it
        //in the database
    }
}
