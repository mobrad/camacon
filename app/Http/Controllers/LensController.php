<?php

namespace App\Http\Controllers;

use App\Lens;

class LensController extends Controller
{
    protected function index()
    {
        return json_encode(Lens::all()->toArray());
    }
}
