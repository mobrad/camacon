<?php

namespace App\Http\Controllers;

use App\PhotographyCategory;
use Illuminate\Http\Request;

class ImageCategoryController extends Controller
{
    //
    public function show(PhotographyCategory $category){
        $images = $category->images;
        return view('category_images',compact('images'));
    }
}
