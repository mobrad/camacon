<?php

namespace App\Http\Controllers;

use App\Photographer;
use App\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    //
    // public function __construct(){
    //  // $this->middleware('api')->except('show');
    // }

    public function show($id)
    {
        $photographer = Photographer::find($id);
        $rating = $photographer->rating();

        return compact('rating');
    }

    public function store($id)
    {
        if (Rating::where([['user_id', '=', request('user_id')], ['photographer_id', '=', request('photographer_id')]])->exists()) {
            return $this->update($id);
        } else {
            return $this->create();
        }
    }

    public function create()
    {
        $rating = Rating::forceCreate([
            'photographer_id' => request('photographer_id'),
            'user_id' => request('user_id'),
            'rating' => request('rating'),
        ])->rating;

        return compact('rating');
    }

    public function update($id)
    {
        $photographer = Photographer::find($id);
        $photographer->rating->update([
            'rating' => request('rating'),
        ]);
        $photographer->save();
        $rating = $photographer->rating->rating;

        return compact('rating');
    }
}
