<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Photographer;
use Auth;
use Illuminate\Http\Request;

class BookingController extends Controller
{

	public function __construct() {
		$this->middleware('auth:web');
	}
    //
    public function show($id){
    	$photographer = Photographer::find($id);
    	return view('booking',compact('photographer'));
    }
    public function store(){
   		//Sets the time 
    	$time = request('time')['date'] .' '. request('time')['time'];
    	
    	Booking::forceCreate([
    		'user_id' => request('user_id'),
    		'photographer_id' => request('photographer_id'),
    		'package_id' => request('plan')['id'],
    		'extra_info' => request('extra'),
    		'shoot_type' => request('type'),
    		'venue' => request('place'),
    		'setting' => request('indoor'),
    		'time' => $time,
    		'payment_method' => request('payment_method')
    	]);
    }
}
