<?php

namespace App\Http\Controllers;

use App\Experience;
use Illuminate\Http\Request;

class ExperienceController extends Controller
{
    //
    public function store(){
    	 	
    	request()->validate([
    		'years' => 'required',
    		'describeExperience' => 'required',
    		'photographerPassion' => 'required',
    		'beginPhotography' => 'required',
    		'loveYourJob' => 'required',
    		'yourShootType' => 'required',
    		'shootDone' => 'required',
    		'favoritePlace' => 'required',
    		'aboutYou' => 'required'
    	]);
        Experience::forceCreate([
           'photographer_id' => request('id'),
           'years_of_experience' => request('years'),
           'describe_experience' => request('describeExperience'),
           'photography_passion' => request('photographerPassion'),
           'begin_photography' => request('beginPhotography'),
           'love_your_job' => request('loveYourJob'),
           'your_shoot_type' => request('yourShootType'),
           'type_of_shoot_done' => request('shootDone'),
           'favorite_place_for_shoot' => request('favoritePlace'),
           'something_about_you' => request('aboutYou') 
        ]);
    }
    public function update($id){
         request()->validate([
        'years' => 'required',
        'describeExperience' => 'required',
        'photographerPassion' => 'required',
        'beginPhotography' => 'required',
        'loveYourJob' => 'required',
        'yourShootType' => 'required',
        'shootDone' => 'required',
        'favoritePlace' => 'required',
        'aboutYou' => 'required'
        ]);
      PhotographerExperience::where('photographer_id',$id)->update([
           'years_of_experience' => request('years'),
           'describe_experience' => request('describeExperience'),
           'photography_passion' => request('photographerPassion'),
           'begin_photography' => request('beginPhotography'),
           'love_your_job' => request('loveYourJob'),
           'your_shoot_type' => request('yourShootType'),
           'type_of_shoot_done' => request('shootDone'),
           'favorite_place_for_shoot' => request('favoritePlace'),
           'something_about_you' => request('aboutYou') 
      ]);
    }
}
