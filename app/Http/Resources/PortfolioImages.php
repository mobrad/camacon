<?php

namespace App\Http\Resources;

use Auth;
use Illuminate\Http\Resources\Json\Resource;

class PortfolioImages extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'src' => $this->url_link(),
            'likes' => $this->likes->count(),
            'user_likes' => false,
            'category' => $this->category->category,
            // $this->when(Auth::guard('photographer')->check(),
        ];
    }
}
