<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public $fillable = ['user_id', 'image_id'];

    protected $guard = ['user_id'];

    /**
     * Checks if the user liked an image.
     *
     * @param int $user_id - the logged in users id
     *
     * @return bool
     **/
    public function user_likes($user_id)
    {
        return $this->where(compact('user_id'))->count() ? true : false;
    }

    public function image()
    {
        return $this->belongsTo(PortfolioImage::class, 'image_id');
    }
}
