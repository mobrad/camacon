<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotographerCamera extends Model
{
    protected $table = 'photographer_camera';

    public $fillable = ['camera_id', 'photographer_id'];

    public function camera()
    {
    }
}
