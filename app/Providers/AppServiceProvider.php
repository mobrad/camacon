<?php

namespace App\Providers;

use App\Package;
use App\PhotographyCategory;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $policies = [
        Photographer::class => PhotographerPolicy::class,
    ];

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::composer('*', function ($view) {
            $categories = PhotographyCategory::all();
            $view->with('categories', $categories);
        });
        View::composer('*', function ($view) {
            $plans = Package::all();
            $view->with('plans', $plans);
        });

        Passport::routes();
    }

    /**
     * Register any application services.
     */
    public function register()
    {
    }
}
