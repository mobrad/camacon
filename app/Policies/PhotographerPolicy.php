<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class PhotographerPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     */
    public function __construct()
    {
    }

    public function is_photographer(Photographer $photographer)
    {
        return '';
    }
}
