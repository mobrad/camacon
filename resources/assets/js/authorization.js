/**
 * Created by Bradley on 09/02/2018.
 */
let  user = window.Laravel.user;
let photographer = window.Laravel.photographer;
module.exports = {
	photographerOwns(model,prop = "photographer_id"){
		return model[prop] === photographer.id
	}
    owns(model,prop = "user_id"){
        return model[prop] === user.id
    },
    isAdmin(){
        return ['Bradley Yarrow'].includes(user.name)
    }
};