/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');

window.Event = new Vue();
window.checkStatus = false;
import StarRating from 'vue-star-rating';
import Vuelidate from 'vuelidate';
import VueCarousel from 'vue-carousel';
import VueFormWizard from 'vue-form-wizard';

import Notifications from 'vue-notification'

window.Typed = require('typed.js');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(VueCarousel);
Vue.use(VueFormWizard);
Vue.use(Vuelidate);
Vue.use(Notifications);
Vue.component('image-check',require('./components/ImageCheck.vue'));
Vue.component('star-rating', StarRating);
Vue.component('basic-info',require('./form/basicInfo.vue'));
Vue.component('equipment',require('./form/equipment.vue'));
Vue.component('BookingForm',require('./form/BookingForm.vue'));
Vue.component('experience',require('./form/Experience.vue'));
Vue.component('portfolio',require('./form/Portfolio.vue'));
Vue.component('PhotographersPortfolio',require('./views/PhotographersPortfolio.vue'));
Vue.component('Registration', require('./views/Registration.vue'));

//shows the unauthenticated notification
window.showUnauthNotification = function showUnauthNotification () {
    window.biscuit.$notify({
        group: 'full-bottom',
        type: 'error',
        text: `Only Users like photographs. 
            <a class="bold" href="/login">Sign In</a>`,
    });
}

window.biscuit = new Vue({
    el: '#root',
    data(){
        return{
            plans: window.Laravel.plans,
            shootTypes: window.Laravel.categories,
            profileFilter: Array(30).fill(""),
            finalModel: {},
            checkStatus:false,
            name:'',
            loadingWizard: false,
            errors: {},
            login: {
                email: "glegros@example.org",
                password: "Mojanity@1",
                remember: false,
                photographer: false,
            },
        }
    },
    computed: {
        hasError() { return !_.isEmpty(this.errors) },
        form() {return {
            url: !this.login.photographer ? '/login' : '/photographer/login',
        }}
    },
    methods: {
        callNotification(group,type,title,text){
            this.$notify({
              group: group,
              title: title,
              text: text,
              type:type
            });
        },
        loginUser() {
            this.errors = {};
            return axios.post(this.form.url, this.login).then(e => {
                window.location = `photographers/${e.data}`;
            }).catch((err) => {
                console.log(err);
                this.errors = err.response.data;
            });
        },
        onComplete: function(){
            var final = this.$refs['portfolio'];
            final.submitPartial();
        },
        validateStep(name) {
            this.name = name;
            var refToValidate = this.$refs[name];
            return refToValidate.submitPartial();
        },
        handleValidation(isValid, tabIndex){
            isValid = true;
        },
        biscuit(id){
            alert(id);a
        },
        save(model,isValid){

            if(isValid){
                var refToSubmit = this.$refs[this.name];

                this.mergePartialModels(model,isValid)
            }

        },
        setLoading(value){
            this.loadingWizard = value;
        },
        
        mergePartialModels(model, isValid){
            if(isValid){
                // merging each step model into the final model

                this.finalModel = Object.assign({},this.finalModel, model)

            }
        }
    }
});

const scrollSpy = () => {
    $('body').delegate('*[wg-scrollspy]', 'click', (evt) => {
        evt.preventDefault();
        let target = $(evt.target).attr('href'), 
            offsetTop = $(evt.target).attr('offset');

        if($(target)[0]) {
            offsetTop = !!parseInt(offsetTop) ? offsetTop : 100;
            $('body, html').animate({ scrollTop: Math.abs(offsetTop - $(target).offset().top)}, 1000, 'swing');
        }
    });
}

$(() => {
    scrollSpy();
})
