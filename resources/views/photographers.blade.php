@extends('layouts.finder')

@section('content')
    @foreach($photographers as $photographer)
    <div class="profile-card">
        <figure>
            <img src="{{asset('/images/concept/concept-5.jpeg')}}" alt="" class="wg-photographer-cover-photo"/>

            <div class="wg-options">
                <a title="View Portfolio" href="{{route('portfolio', $photographer)}}">
                    <button ghost class="wg-portfolio"><i class="wgi wgi-portfolio"></i></button>
                </a> <br/>
                <button title="Book" ghost class="wg-book"><i class="wgi wgi-booking"></i></button>
            </div>
        </figure>
        <div class="wg-photographer-info">
            <img title="" src="{{asset('/images/avatar.png')}}" alt="" class="wg-photographer-avatar img-circle">
            {{ $photographer->studio_name }}
            <small class="wg-text-primary pull-right">4.5</small>
        </div>
    </div>
    @endforeach

@endsection
