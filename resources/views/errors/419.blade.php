@extends('layouts.app')

@push('style')
	<link rel="stylesheet" href="{{ asset('/css/themify.css')}}"/>
@endpush

@section('content')
	<div class="container wg-error">
		<div class="text-center">
			<h1 class="wg-heading">Your Session Expired</h1>
			<h4>Your login session is expired and you'll need to <a href="{{ route('login') }}">Sign In</a></h4>
			<section>
				<img src="/images/assets/404.svg">			
			</section>
		</div>
	</div>
	<br>
@stop
