@extends('layouts.app')

@push('style')
	<link rel="stylesheet" href="{{ asset('/css/themify.css')}}"/>
@endpush

@section('content')
	<div class="container wg-error">
		<article class=wg-error-page>
			<h1>404 - <small>PAGE NOT FOUND</small></h1>
			<h3>Help & Support</h3>
			<p>The page you are looking for is no longer here. Maybe it was here in the first place. In any case we are sorry.</p>
			<div class="">
				<button class="btn"><i class="ti ti-email"></i> Send an Email</button>
				<button class="btn"><i class="ti ti-twitter"></i> Twitter Us @Spectrang</button>
				<button class="btn"><i class="mdi mdi-bug"></i> Report a Bug</button>
			</div>
		</article>
		<div class="wg-error-image">
			<img src="/images/assets/404.svg">
		</div>
	</div>
	<br>
@stop
