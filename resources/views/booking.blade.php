@extends('layouts.app')

@push('link')
<link rel="stylesheet" href="{{asset('css/vue-form-wizard.min.css')}}">
<link rel="stylesheet" href="{{asset('css/themify.css')}}">
@endpush

@section('content')
    <div class="container">
        <div class="wg-booking wg-card" inline-template is="booking-form">
        <!--start-index="1" -->
        <form-wizard v-cloak color="crimson" @on-complete="bookingCompleted" title="Book a Photographer in Minutes" 
            subtitle="Your perfect photographer is just a few taps away.">
            <tab-content :before-change="() => firstDone()" title="Tell us about the shoot" icon="ti-user">

                <transition-group name="fade">
                <div :key="5" class="shoot-type text-center">
                    <h3 class="mt-30">Pick a Shoot Plan 
                        <button 
                            v-if="queue.plan"
                            title="Reset Plan" ghost=""
                            @click="(() => form.plan = {})"
                            >
                            <i class="ti ti-reload"></i>
                        </button>
                    </h3>
                    <center v-if="!$root.plans.length">We couldn't find a Shooting Plan.</center>
                    <div class="wg-plan">
                        <div :key="index" class="wg-plan-card btn-special"
                            :class="{'wg-active': form.plan.plan_name === plan.plan_name }"
                            v-for="(plan, index) in $root.plans" 
                            @click="setPlan(plan)">
                            <article>
                                <h3>@{{plan.plan_name}}</h3>
                                <h5 class="wg-t-primary">₦@{{ plan.plan_price }}</h5>
                            </article>
                        </div>
                    </div>
                </div>

                <div :key="0" v-if="queue.plan" class="shoot-type text-center">
                    <h3 class="mt-30">What type of Shoot</h3>
                    <center v-if="!!$root.shootTypes">We current have no Shoot Type! Please try again later.</center>
                    <button :key="index" v-for="(shootType, index) in $root.shootTypes"
                        class="btn-special" :class="{'wg-active': form.type==shootType.name }" @click="setType(shootType)">@{{shootType.name}}</button>
                </div>

                <div :key="1" v-if="queue.type" class="place text-center">
                    <h4 class="mr">Where is the shooting taking place?</h4>
                    <form action="">
                        <input type="hidden"  v-model="form.photographer_id = {{$photographer->id}}">
                        <input type="text" class="form-control" v-model="form.place" placeholder="Shoot Venue...">
                    </form>
                </div>

                <div :key="2" v-if="form.place.length" class="outdoor text-center">
                    <h4 class="mr">Is it Indoor or Outdoor</h4>
                    <button type="button" @click="setIndoor('Indoor')" :class="{'wg-active': form.indoor == 'Indoor'}" class="btn-special">Indoor </button>
                    <button type="button" @click="setIndoor('Outdoor')" :class="{'wg-active': form.indoor == 'Outdoor'}" class="btn-special">Outdoor </button>
                    <button type="button" @click="setIndoor('Both')" :class="{'wg-active': form.indoor == 'Both'}" class="btn-special">Both </button>
                    <button type="button" @click="setIndoor('Not Sure')" :class="{'wg-active': form.indoor == 'Not Sure'}" class="btn-special">Not sure </button>
                </div>
                
                <div :key="3" v-if="queue.indoor" class="date text-center">
                    <h4 class="mr">When do you need the photographer?</h4>
                    <form action="">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="date" v-model="form.time.date" class="form-control" placeholder="Pick a date">
                            </div>
                            <div class="col-md-6">
                                <input type="time" v-model="form.time.time" class="form-control" placeholder="Select Time">
                            </div>
                        </div>
                    </form>
                </div>

                <div :key="4" v-if="queue.time" class="extra-info text-center">
                    <h4 class="mr">Is there any thing you'd Like us to know about?</h4>
                    <form action="">
                        <input type="text" v-model="form.extra" class="form-control" placeholder="any extra information">
                    </form>
                </div>
                </transition-group>
            </tab-content>

            <tab-content title="Last step" icon="ti-check">

                <h2 class="mr text-center">Booking Receipt</h2>
                <section class="wg-receipt">
                    <table class="table table-responsive">
                        <tbody>
                            <tr>
                                <td>PLAN</td>
                                <td>@{{ form.plan.plan_name}} = ₦@{{ form.plan.plan_price}}</td>
                            </tr>
                            <tr>
                                <td>LOCATION</td>
                                <td>@{{ form.place}}</td>
                            </tr>
                            <tr>
                                <td>SHOOT TIME</td>
                                <td>
                                    <span>Time</span> <b>@{{ form.time.time}}</b> <br/>
                                    <span>Date</span> <b>@{{ form.time.date}}</b>
                                </td>
                            </tr>
                            <tr>
                            </tr>

                        </tbody>
                    </table>
                    <h5>SHOOT DESCRIPTION</h5>
                    <p>
                        Shots is to be taken @{{form.indoor}} at @{{form.time.time}} on the @{{ form.time.date }}, in a place called 
                        <b>@{{ form.place }}</b>. The type of shoot to be taken is <i v-text="form.type"></i> Shots.
                    </p>
                </section>
                <h2 class="mr text-center">Payment</h2>
                <div class="form-group">
                    <select v-model="form.payment_method" class="form-control">
                        <option value="card">Credit Card</option>
                        <option value="transfer">Bank Transfer</option>
                        <option value="cash">Cash</option>
                    </select>
                </div>
                <div v-if="queue.payment_method.transfer">
                    <table class="table">
                        <tr>
                            <th>
                                Account
                            </th>
                            <th class="text-left">Bank</th>
                        </tr>
                        <tbody>
                            <tr>
                                <td>
                                    <small>Spectra Inc.</small> <br> 6083988893819
                                </td>
                                <td>
                                    Zenith Bank
                                </td>
                            </tr>
                        </tbody>
                    </table>                    
                </div>
            </tab-content>
        </transition>
        </form-wizard>
        </div>
    </div>
@endsection
