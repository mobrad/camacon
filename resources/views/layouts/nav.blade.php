@push('photographer-nav') 
@auth('photographer') 
    <li class="wg-avatar-thumbnail dropdown">
        <img src="{{ asset('images/avatar.png') }}" data-toggle="dropdown" width="30" height="30" />
        <ul class="dropdown-menu slide-up dropdown-menu-right">
            <li><a href="{{ route('photographer.portfolio') }}">
                <i class="ti ti-briefcase"></i> Portfolio</a>
            </li>
            <li><a href="#"><i class="ti ti-settings"></i> Settings</a></li>
            <li><a href="#" onclick="$('form#logout').submit()"><i class="ti ti-arrow-right"></i> Logout</a></li>
        </ul>
    </li>
@endauth
@endpush

@push('client-nav')
@auth('web') 
        <li class="wg-avatar-thumbnail dropdown">
            <img src="{{ asset('images/avatar.png') }}" data-toggle="dropdown" width="30" height="30" />
            <ul class="dropdown-menu slide-up dropdown-menu-right">
                <div class="dropdown-header text-right"> {{ auth()->user()->fullname() }} </div>
                <li><a href="#"><i class="ti ti-briefcase"></i> Gallery</a></li>
                <li><a href="#"><i class="ti ti-settings"></i> Booking History</a></li>
                <li><a href="#" onclick="$('form#logout').submit()"><i class="ti ti-arrow-right"></i> Logout</a></li>
            </ul>
        </li>
@endauth
@endpush

<header>
    <nav>
        <div class="icon"><a href="/" ghost title="Spectra Home">Spectra</a></div>
        @if (Route::has('login'))
            <div class="wg-nav-links">
                <li><a href="{{ route('how-it-works') }}">How it Works</a></li>

                @if(auth()->guard('photographer')->guest())
                    <li><a href="{{ route('photographer-finder') }}">Book now</a></li>
                @endif

                @guest('web')
                    @guest('photographer') 
                    <li><a href="{{ route('login') }}"><i class="ti ti-user"></i> Login</a></li>
                    @endguest
                @endguest

                @stack('client-nav')
                @stack('photographer-nav')
            </div>
        @endif
    </nav>
    <form id="logout" method="POST" action="{{ route('logout')}}">
        {{ csrf_field() }}
    </form>
</header>
