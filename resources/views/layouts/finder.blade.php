<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Book Photographer | Spectra </title>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'photographer_id' => null,
        ]) !!};
    </script>
    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('css/themify.css') }}"/>
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    {{-- stack link for (link tags) external files --}}
    @stack('link')
</head>

<body>
    <div id="root" class="wg-finder">
        @include('layouts.nav')
        
        <main id="wg-content">
            <div id="wg-profile-directory">
                <div class="wg-search-info text-right">
                    <b v-cloak>@{{ profileFilter.length }} Photographers Found.</b>
                </div>
                <div id="wg-profile-directory-list">
                    @yield('content')
                </div>
            </div>

            <section id="wg-sidebar">
                <section class="search-box">
                    <div class="wg-search wg-full-width">
                        <input type="text" placeholder="Search photographer">
                        <button><i class="ti ti-search"></i></button>
                    </div>
                </section>

                <section class="wg-filter">
                    <div v-for="a in 10">
                        <h5 class="wg-filter-heading">Photographer Types @{{a}}</h5>
                        <ul class="wg-filter-btn-list">
                            <li class="">Portriat</li>
                            <li class="">Landscape</li>
                            <li class="">Graduation</li>
                            <li class="">Wild Life</li>
                            <li class="">Business</li>
                        </ul>
                    </div>        
                </section>
            </section>
        </main>
    </div>

    {{-- javascript files --}}
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('script')
</body>
</html>
