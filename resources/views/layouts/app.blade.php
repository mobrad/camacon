<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Spectra</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Karla:400,700|Raleway:300,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/themify.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    @stack('link')

</head>

<script>
    const Laravel = Object.freeze({!! json_encode([
       'csrfToken' => csrf_token(),
       'photographer_id' => auth('photographer')->id(),
       'categories' => $categories,
       'plans' => $plans,
       'user_id' => auth()->id(),
    ]) !!});
    window.Laravel = Laravel;
</script>
@stack('style')
<body>
<div id="root">
    @include('layouts.nav')
    @yield('background-caption')
    @yield('content')

    <footer>
        <button id="top" wg-scrollspy href="#root">
            <i class="ti ti-angle-double-up"></i>
        </button>
        <div class="col-md-12" style="background:white;padding:70px">
            <div class="col-md-3">
                <ul class="footer-nav">
                    <h3>Company</h3>
                    <li>About us</li>
                    <li>Blog</li>
                    <li>Get in touch</li>
                    <li>Terms and Policies</li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="footer-nav">
                    <h3>Support</h3>
                    <li>How it works</li>
                    <li>Pricing</li>
                    <li>FAQs</li>
                    <li>Help Center</li>
                    <li>Photographer Login</li>
                    <li>Become a Pro</li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="footer-nav">
                    <h3>Popular</h3>
                    <li>Business events </li>
                    <li>Professional Headshot</li>
                    <li>Party Photography</li>
                    <li>family retreat</li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="footer-nav" id="no-heading">
                    <h3>    </h3>
                    <li>Kids Party</li>
                    <li>Birthday parties</li>
                    <li>Wedding & Engagements</li>
                </ul>
            </div>
        </div>
        <div class="col-md-12">
            Copyright
        </div>
    </footer>
    <notifications 
    :speed="400" :duration="7000" 
    class="portfolio-notify" group="full-bottom" 
    width="350" position="bottom center" />
</div>
</body>
<script src="{{ asset('js/app.js') }}"></script>
@stack('script')
</html>
