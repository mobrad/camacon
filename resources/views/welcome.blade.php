@extends('layouts.app')

@section('background-caption')
    <section class="main-bg">
        <div class="main-caption">
            <section>
                <h1 class="heading">Hire a professional Photographer</h1>
                <p>browsing through the best concepts.</p>
            </section>

            <div>
                <a href="{{route('photographer.register')}}"><button class="btn-pink">Become a Photographer</button></a>
                <a href="{{route('photographer-finder')}}"><button class="btn-white">Hire Photographer</button></a>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <main>
        <div class="container-fluid">
            <div>
                <div id="wg-steps" style="padding-bottom:40px;">
                    <div class="wg-step">
                        <img src="/images/assets/wg-easy.png" class="wg-step-image" alt="">
                        <h3 class="heading">Easy</h3>
                        <p class="wg-step-content">Book for a photographer in seconds, its that fast and easy.</p>
                    </div>
                    <div class="wg-step">
                        <img src="/images/assets/wg-assure.png" class="wg-step-image" alt="">
                        <h3 class="heading">Happiness Assured</h3>
                        <p>Simply pick a package and have an experienced photographer booked instantly, then receive edited photos within 48hrs after shoot. Our large network guarantees 100% availability</p>
                    </div>
                    <div class="wg-step">
                        <img src="/images/assets/wg-money.png" class="wg-step-image" alt="">
                        <h3 class="heading">Save money</h3>
                        <p>Experience a <big>2%</big> discount, when you pay online on Spectra</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="">
            <h2 class="cnt-h1">Professional Photography Service on Demand</h2>
            <p class="subheading" >Be at a conference at House of Lords or your little one's 1st birthday, Spectra has it covered!</p>
        </div>

        <div class="photo-category">
            <p id="personal">Personal</p>
            <p id="business">Business</p>
        </div>
        <div class="container">
            <div class="row row-grid" style="display: flex; justify-content: space-around; flex-wrap:wrap;">
               @foreach($categories as $category)
                    <image-check cat-id="{{$category->id}}" cat-name="{{$category->name}}" color="white" title="{{$category->name}}" @click="biscuit"></image-check>
                @endforeach
            </div>
        </div>
        <figure class="leavee-bg">
            <h1 class="text-center caption">Spectra makes booking a photographer as <br> as easy as hailing an Uber.</h1>
        </figure>
        <div class="container-fluid testimonial">
            <div class="row">
                <div class="text-center">
                    <h2 class="heading">Don't take us by our word</h2>
                    <p style="margin-top: 20px;">See why customers like spectra</p>
                </div>
                <div v-cloak class="testimonial text-center">
                    <carousel :per-page="1" :navigate-to="someLocalProperty">
                        <slide>
                            <img class="img-responsive" src="images/concept/concept-1.jpeg" alt="" width="300" height="260">
                            <img class="img-responsive" src="images/concept/concept-3.jpeg" alt="" width="300" height="260">
                            <img class="img-responsive" src="images/concept/concept-2.jpeg" alt="" width="300" height="290">
                            <div class="comment">
                                <p>This guys really know their work and i'll keep choosing them over them,This guys really know their work and i'll keep choosing them over them This guys really know their work and i'll keep choosing them over them</p>
                                <figure>
                                    <img class="comment-avatar" src="images/concept/concept-2.jpeg">
                                    <figcaption>Bradley Yarrow</figcaption>
                                </figure>
                            </div>
                        </slide>

                        <slide>
                            <img class="img-responsive" src="/images/concept/concept-7.jpeg" alt="" width="300" height="260">
                            <img class="img-responsive" src="/images/concept/concept-5.jpeg" alt="" width="300" height="260">
                            <img class="img-responsive" src="/images/concept/concept-6.jpeg" alt="" width="300" height="290">
                            <div class="comment">
                                <p>Spectra the best online photography platform. I got really cool shoots for an accordable price.</p>
                                <figure>
                                    <img class="comment-avatar" src="/images/avatar.png">
                                    <figcaption>Joseph Owonvwon</figcaption>
                                </figure>
                            </div>
                        </slide>
                    </carousel>
                </div>
            </div>
        </div>
        <div class="container-fluid pricing">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="heading">Fixed,Simple and affordable Pricing</h1>
                    <h4>Our innovative model reduces costs for photographers, so you save £££ without compromising on quality.</h4>
                </div>
                <div class="col-md-12 text-center flying-ribbon" style="padding: 70px">
                    <carousel :per-page="6" :navigate-to="someLocalProperty">
                        <slide>
                            <li class="price-slide">
                                <div>
                                    <h5>Flash</h5>
                                    <h2>50</h2>
                                    <p>30 minutes shoot unlimited photo all edited</p>
                                    <button class="btn btn-pink btn-expand">Book</button>
                                </div>
                            </li>

                        </slide>
                        <slide>
                            <li class="price-slide">
                                <div>
                                    <h5>Flash</h5>
                                    <h2>50</h2>
                                    <p>30 minutes shoot unlimited photo all edited</p>
                                    <button class="btn btn-pink btn-expand">Book</button>
                                </div>
                            </li>

                        </slide>
                        <slide>
                            <li class="price-slide">
                                <div>
                                    <h5>Flash</h5>
                                    <h2>50</h2>
                                    <p>30 minutes shoot unlimited photo all edited</p>
                                    <button class="btn btn-pink btn-expand">Book</button>
                                </div>
                            </li>

                        </slide>
                        <slide>
                            <li class="price-slide">
                                <div>
                                    <h5>Flash</h5>
                                    <h2>50</h2>
                                    <p>30 minutes shoot unlimited photo all edited</p>
                                    <button class="btn btn-pink btn-expand">Book</button>
                                </div>
                            </li>

                        </slide>
                    </carousel>
                </div>
                <div class="col-md-12 text-center">
                    <div class="col-md-4"><h3>All packages include</h3></div>
                    <div class="col-md-4">
                        <ul class="no-style">
                            <li><span class="space"><i class="glyphicon glyphicon-ok"></i></span>Fully vetted photographer</li>
                            <li><span class="space"><i class="glyphicon glyphicon-ok"></i></span>Instant booking</li>
                            <li><span class="space"><i class="glyphicon glyphicon-ok"></i></span>Guranteed availability</li>
                            <li><span class="space"><i class="glyphicon glyphicon-ok"></i></span>Unlimited no. of photos</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="no-style">
                            <li><span class="space"><i class="glyphicon glyphicon-ok"></i></span>Professional editing</li>
                            <li><span class="space"><i class="glyphicon glyphicon-ok"></i></span>48 hours delivery</li>
                            <li><span class="space"><i class="glyphicon glyphicon-ok"></i></span>Private online gallery</li>
                            <li><span class="space"><i class="glyphicon glyphicon-ok"></i></span>Multiple-sized images</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12 text-center" style="margin-top:60px;margin-bottom: 60px;">
                    <h1 class="cnt-h1">Hire a photographer </h1>
                    <h2>For your <span class="wg-t-blue" id="typed"></span></h2>
                    <div>
                        <button class="btn-pink">Get Price</button>
                        <button class="btn-white">Hire a photographer</button>
                    </div>

                </div>
            </div>
        </div>
    </main>
    @endsection

@push('script')
<script>
    var options = {
        strings: ["Birthday Party.", "Church Event.","Personal Photo","Wedding Photo"],
        smartBackspace: true,
        typeSpeed: 40,
        loop: true
    }

    var typed = new Typed("#typed", options);
</script>
@endpush
