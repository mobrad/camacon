@extends('layouts.app')

@section('content')
    <section inline-template v-cloak is="PhotographersPortfolio" :photographer_id="{{$photographer->id}}">
        <div>
            <section id="portfolio-container" class="mt-30">
                <aside class="portfolio-sidebar">
                    <section class="profile">
                        <div class="text-muted wg-small-caps text-right">
                            <button ghost="true">EDIT</button>
                        </div>
                        <div class="text-center">
                            <img src="{{asset('images/avatar.png')}}" class="profile-pic" alt="profile picture">
                            <div class="booking-hold text-center">
                                <button title="Book {{ $photographer->studio_name}}" 
                                    class="btn booking-btn"><a href="{{route('booking', $photographer->id)}}"><i class="wgi wgi-booking"></i></a></button>
                            </div>
                        </div>
                        <div class="panel-body text-center">
                            <h4 class="profile-name">{{$photographer->full_name}}</h4>
                            <h5>{{ strtoupper($photographer->studio_name) }}</h5>
                        </div>
                        <star-rating active-color="#bdddac" @rating-selected ="setRating" :rating="rating" :star-size="20" style="justify-content: center;" :show-rating="false"></star-rating>
                    </section>

                    <section class="mt-30 wg-equipments">
                        <h4 class="mt-0"><b>Equipments</b> 
                            @if(@auth('photographer')->user()->id === $photographer->id):
                            <div class="pull-right mt-i-10" data-target="#wg-edit-modal" data-toggle="modal">
                                <button class="md-btn-circle"><i class="ti ti-plus"></i></button>
                            </div>
                            @endif
                        </h4>
                        <h5>Cameras</h5>
                        <span v-for="(cam, i) in equipments.cameras" class="badge m-5">@{{ cam.name }}</span>
                        <h5>Lenses</h5>
                        <span v-for="(lens, i) in equipments.lenses" class="badge m-5">@{{ lens.name }}</span>
                    </section>
                </aside>
                <div class="portfolio-records">
                     <!-- Nav tabs -->
                    <ul class="speciality text-center mt-3" role="tablist">
                        <li role="presentation" :class="{'active': isCurrent(catName)}"  
                            v-for="(a, catName) in portfolioCategory">
                            <a class="" :href="'#' + catName" @click="pick(catName)" :aria-controls="catName" 
                            role="tab" data-toggle="tab"> @{{catName}}
                            </a>
                        </li>
                    </ul>
                    <transition name="slideup">
                        <div v-show="showLoader" class="preloader-holder text-center">
                            <wg-preloader color="crimson" size="lg"/>                                    
                        </div>
                    </transition>
                    <transition name="slideDown">
                        <div v-show="!showLoader && categoryKeys.length === 0">
                            <h4 class="text-center">
                                No Concept to <b>Display</b>
                            </h4>
                        </div>
                    </transition>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <section role="tabpanel" :class="{'active': isCurrent(key)}" 
                            class="tab-pane" :id="key" 
                            v-for="(category, key) in portfolioCategory">
                            <div class="wg-user-portfolio">
                                <transition name="slideDown" :key=key
                                    v-for="(image, key) in category">
                                    <div v-if="imagesFetched"  @click="viewFullScreen(key)"
                                        class="item" :style="{backgroundImage: `url(${image.src})`}">
                                        <like-button :image_id="image.id" :status="image.user_likes" :image_likes='image.likes'></like-button>
                                    </div>
                                </transition>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
           
            <div id="wg-edit-modal" class="modal slide-up">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title">
                                <i class="ti ti-camera pull-left mx-15" style="font-size: 2em"></i> 
                                <div class="pull-left">
                                    ADD INVENTORY. <br> 
                                    <small>Add your photography equipments to the list.</small>
                                </div>
                                <button ghost data-dismiss="modal" data-target="#wg-edit-modal" class="pull-right">
                                    <i class="ti ti-close" style="font-size: 1.5em;"></i>
                                </button>
                            </div>
                        </div>
                        <div class="modal-body">
                            <equipment :equipments="equipments" ref="equipments"></equipment>
                        </div>
                        <div class="modal-footer text-left">
                            <button @click="showRefs" class="btn md-btn"><i class="ti ti-save"></i>  Save</button>
                        </div>
                    </div>
                </div>
            </div>

            <wg-lightbox @closed="(e) => lightbox.show = e" :current-index="lightbox.index" :show="lightbox.show" :images="getCurrent"/>    
        </div>
    </section>

@endsection
