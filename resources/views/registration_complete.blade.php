@extends('layouts.app')

@section('content')
    <main style="margin: 100px;">
        <div class="complete">
            <i class="glyphicon glyphicon-check good"></i>
            <h1>Please confirm your email address</h1>
            <p>An email has been sent to the mail you gave us</p>
            <p><a href="">Click here to resend the email</a></p>
        </div>
    </main>
@endsection
