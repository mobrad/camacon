@extends('layouts.app')

@push('link')
    <link rel="stylesheet" href="{{asset('css/vue-form-wizard.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/themify.css')}}">
@endpush

@section('content')

    <main>
        <div class="hero-caption">
            <div class="photographer"></div>
        </div>
        <div class="container" style="padding: 50px;">
            <h1 class="spectra text-center">Become a Spectra Photographer</h1>
            <div class="hero-form">
                <form-wizard  color="crimson" :start-index="3" @on-complete="onComplete" title="" subtitle="" @on-loading="setLoading">

                <tab-content title="Personal details" icon="ti-user" :before-change="()=>validateStep('basic-info')">
                    <basic-info ref="basic-info" @on-validate="biscuit"></basic-info>
                </tab-content>
                
                {{--Equipments Tab Section--}}
                <tab-content title="Equipments" icon="ti-settings" :before-change="()=>validateStep('equipment')">
                    <div v-cloak>
                        <p class="text-center">Let us know the type of equipments you make use of</p>
                        <hr class="pageLead">
                    </div>
                    <equipment ref="equipment"></equipment>
                </tab-content>
                {{--End of equipment tab section--}}

                {{--Experience tab section--}}
                <tab-content title="Experience" icon="ti-check" :before-change="()=>validateStep('experience')">
                    <experience ref="experience"></experience>
                </tab-content>
                {{--End of experience tab section--}}

                {{--Porfolio Tab--}}
                <tab-content title="Portfolio" icon="ti-check">
                    <portfolio ref="portfolio"></portfolio>
                </tab-content>

                {{--End of portfolio tab section--}}
                </form-wizard>

            </div>
        </div>
        <notifications group="error"/>
        <notifications group="success"/>
    </main>
@endsection
