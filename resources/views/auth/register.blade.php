@extends('layouts.app')

@section('content')
<div class="container">
    <div id="wg-register-box">
        <h3 class="heading">Register</h3>
        <h5 class="mb-30">Simple fill the form to get started!</h5>

        <registration class="wg-form-holder" inline-template>
            <section>
                <form class="" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <label for="name" class="control-label">First Name</label>

                        <div>
                            <input id="first_name" type="text" class="form-control" 
                                name="first_name" value="{{ old('first_name') }}" required autofocus>

                            @if($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <label for="name" class="control-label">Last Name</label>

                        <div>
                            <input id="last_name" type="text" class="form-control" 
                                name="last_name" value="{{ old('last_name') }}" required autofocus>

                            @if($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label">E-Mail Address</label>

                        <div>
                            <input id="email" type="email" class="form-control" 
                                name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                        <label for="telephone" class="control-label">Telephone</label>

                        <div>
                            <input id="telephone" type="text" class="form-control" name="telephone" value="{{ old('telephone') }}" required autofocus>

                            @if ($errors->has('telephone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('telephone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <label for="address" class="control-label">Address</label>

                        <div>
                            <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus>

                            @if ($errors->has('address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label">Password</label>

                        <div>
                            <input id="password" type="password" v-model="form.password" class="form-control" name="password" required>
                            <password-validator :user-inputs="[form.first_name,form.email, form.last_name]"style="width: 100%" v-model="form.password"/>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <transition name="fade">
                        <div v-if="form.password.length > 4" class="form-group">
                            <label for="password-confirm" class="control-label">Confirm Password</label>

                            <div>
                                <input id="password-confirm" v-model="form.cpassword" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                    </transition>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </form>
                <div class="cover-image">
                    <img style="width: 100%;height: 100%;object-fit: cover;" src="{{asset('images/photographer.jpg')}}"/>
                </div>
            </section>
        </registration>
    </div>
</div>
@endsection
    
