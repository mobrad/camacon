@extends('layouts.app')

@section('content')
    <div class="container">
        <div id="wg-login-box">
            <div class="back-images">
                <figure id="wg-photographer-image">
                </figure>
                <figure id="wg-client-image">
                </figure>
            </div>

            <form :class="{right: login.photographer}" method="POST" @submit.prevent="loginUser()" action="{{ route('login') }}">
                {{ csrf_field() }}
                <h3 v-cloak class="heading">@{{ !login.photographer ? 'User' : "Photographer" }} Login
                </h3>
                <div class="mb-30" v-cloak>
                    <span v-if="login.photographer">Not a Photographers?</span>
                    <span v-else>Are you a Photographers?</span>
                    <a @click="login.photographer = !login.photographer" href="javascript: void (0)">Login Here</a>
                </div>
                <div v-if="hasError" v-cloak class="alert alert-danger">
                    Invalid email or password entered.
                </div>
                <div class="form-group">
                    <input id="email" type="email" placeholder="Email Address..." 
                        class="form-control"
                        v-model.trim="login.email" required autofocus>
                </div>

                <div class="form-group">
                    <input id="password" placeholder="Password..." 
                        type="password" class="form-control" 
                        v-model.trim="login.password" required>
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input v-model="login.remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="wg-btn btn-pink">
                        Login
                    </button>

                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('script') 
    <script>
        $('#wg-login-box a[toggle-login]').click(function() {
            $('#wg-login-box > form').toggleClass('right');
        });
    </script>
@endpush
