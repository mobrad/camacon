<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotographersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photographers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('studio_name');
            $table->string('tel');
            $table->string('city');
            $table->string('email')->unique();
            $table->string('address');
            $table->boolean('confirmed')->default(false);
            $table->string('confirmation_token',25)->nullable()->unique();
            $table->string('password');
            $table->string('shoot_hour');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photographers');
    }
}
