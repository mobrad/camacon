<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotographerCategory extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('photographer_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('photographer_id')->unsigned()->index();
            $table->foreign('photographer_id')->references('id')->on('photographers')->onDelete('cascade');

            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('photography_categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('photographer_category');
    }
}
