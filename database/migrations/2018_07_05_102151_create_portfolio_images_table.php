<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioImagesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('portfolio_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('photography_category_id')->unsigned();
            $table->foreign('photography_category_id')->references('id')->on('photography_categories')->onDelete('cascade');
            $table->integer('photographer_id')->unsigned();
            $table->foreign('photographer_id')->references('id')->on('photographers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_images');
    }
}
