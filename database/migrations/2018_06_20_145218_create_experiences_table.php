<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('photographer_id')->unsigned();
            $table->foreign('photographer_id')->references('id')->on('photographers')->onDelete('cascade');
            $table->integer('years_of_experience');
            $table->string('describe_experience');
            $table->string('photography_passion');
            $table->string('begin_photography');
            $table->string('love_your_job');
            $table->string('your_shoot_type');
            $table->string('type_of_shoot_done');
            $table->string('favorite_place_for_shoot');
            $table->string('something_about_you');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
}
