<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotographerSpecialCamera extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('photographer_special_camera', function (Blueprint $table) {
            $table->integer('photographer_id')->unsigned()->index();
            $table->foreign('photographer_id')->references('id')->on('photographers')->onDelete('cascade');

            $table->integer('special_camera_id')->unsigned()->index();
            $table->foreign('special_camera_id')->references('id')->on('special_cameras')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('photographer_special_camera');
    }
}
