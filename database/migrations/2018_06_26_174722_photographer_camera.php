<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotographerCamera extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('photographer_camera', function (Blueprint $table) {
            $table->integer('photographer_id')->unsigned()->index();
            $table->foreign('photographer_id')->references('id')->on('photographers')->onDelete('cascade');

            $table->integer('camera_id')->unsigned()->index();
            $table->foreign('camera_id')->references('id')->on('cameras')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('photographer_camera');
    }
}
