<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotographerLens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('photographer_lens', function (Blueprint $table) {
            $table->integer('photographer_id')->unsigned()->index();
            $table->foreign('photographer_id')->references('id')->on('photographers')->onDelete('cascade');

            $table->integer('lens_id')->unsigned()->index();
            $table->foreign('lens_id')->references('id')->on('lens')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('photographer_lens');
    }
}
