<?php

use App\Lens;
use App\Camera;
use App\Photographer;
use App\PhotographerLens;
use App\PhotographerCamera;
use Illuminate\Database\Seeder;

class PhotographerTableSeeder extends Seeder
{
    public $lens = [];
    public $camera = [];

    public function __construct()
    {
        $this->lens = Lens::all()->take(5)->pluck(['id'])->toArray();
        $this->camera = Camera::all()->take(5)->pluck(['id'])->toArray();
    }

    /**
     * Run the database seeds.
     */
    public function run()
    {
        Photographer::truncate();

        $user = factory(Photographer::class)->create([
            'full_name' => 'Bradford Parker',
            'studio_name' => 'Park Shots',
            'email' => 'rhaley@yahoo.com',
            'password' => bcrypt('tommy'),
            'address' => '20th Cast Avenue, By Ken Lukes Church.',
        ]);

        $this->pick_lens($user->id);
        $this->pick_cameras($user->id);

        factory(Photographer::class, 50)->create();
    }

    protected function pick_cameras($user_id)
    {
        PhotographerCamera::truncate();

        $cameras = $this->camera;
        //assign 5 cameras user
        foreach ($cameras as $camera) {
            factory(PhotographerCamera::class)->create([
                'photographer_id' => $user_id,
                'camera_id' => $camera,
            ]);
        }
    }

    protected function pick_lens($user_id)
    {
        PhotographerLens::truncate();

        $lens = $this->lens;
        shuffle($lens);
        //assign 5 lens user
        foreach ($lens as $len) {
            factory(PhotographerLens::class)->create([
                'photographer_id' => $user_id,
                'lens_id' => $len,
            ]);
        }
    }
}
