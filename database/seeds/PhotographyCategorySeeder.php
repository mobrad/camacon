<?php

use App\PhotographyCategory;
use Illuminate\Database\Seeder;

class PhotographyCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        PhotographyCategory::truncate();

        $categories = ['Wedding', 'Dance', 'Birthday', 'Graduation', 'Church'];
        foreach ($categories as $name) {
            factory(PhotographyCategory::class)->create(compact('name'));
        }
    }
}
