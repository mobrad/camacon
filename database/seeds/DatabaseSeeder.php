<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        $this->call(array(
//            PackageTableSeeder::class,
//            UserTableSeeder::class,
//            PhotographyCategorySeeder::class,
//            PhotographerTableSeeder::class,
//            PhotographerCategorySeeder::class,
//            CameraLenSeeder::class,
            PortfolioImageSeeder::class
        ));

        Schema::enableForeignKeyConstraints();
    }
}
