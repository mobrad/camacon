<?php

use App\Photographer;
use App\PhotographyCategory;
use App\PhotographerCategory;
use Illuminate\Database\Seeder;

class PhotographerCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        PhotographerCategory::truncate();
        $photographers = Photographer::all()->take(20);
        $categories = PhotographyCategory::take(3)->get()->pluck('id')->toArray();

        $photographers->map(
            function ($ph) use ($categories) {
                foreach ($categories as $category_id) {
                    PhotographerCategory::create([
                        'photographer_id' => $ph->id,
                        'category_id' => $category_id,
                    ]);
                }
            }
        );
    }
}
