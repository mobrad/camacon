<?php

use App\PortfolioImage;
use App\PhotographyCategory;
use Illuminate\Database\Seeder;

class PortfolioImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        PortfolioImage::truncate();

        $categories = PhotographyCategory::take(3)->get()->pluck('id')->toArray();
        array_map(
            function ($value) {
                factory(PortfolioImage::class, 3)->create([
                    'name' => "concept-{$value}.jpeg",
                    'photographer_id' => 1,
                    'photography_category_id' => $value,
                ]);
            }, $categories
        );
    }
}
