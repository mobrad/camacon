<?php

use App\Package;
use Illuminate\Database\Seeder;

class PackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Package::truncate();
        $packages = ['Small', 'Business', '24 Hrs', '8 Hrs'];

        foreach ($packages as $plan_name) {
            factory(Package::class)->create(compact('plan_name'));
        }
    }
}
