<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        User::truncate();
        factory(User::class)->create([
            'email' => 'owonwo@gmail.com',
            'first_name' => 'Joseph',
            'last_name' => 'Julius',
            'password' => bcrypt('tommy'),
        ]);
        factory(User::class, 30)->create();
    }
}
