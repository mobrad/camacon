<?php

use App\Lens;
use App\Camera;
use Illuminate\Database\Seeder;

class CameraLenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Lens::truncate();
        Camera::truncate();

        $lenses = ['Focal 5', 'Fish Eye ||', 'Mark 3 Duoes', 'Zoom ||', 'Zoom |'];
        $camera = ['Canon 3d', 'Canon 7D 40S', 'Nikon 506', 'Kodak 10MP', 'Samsung GT-30'];

        foreach ($lenses as $key => $name) {
            factory(Lens::class)->create(compact('name'));
            factory(Camera::class)->create(['name' => $camera[$key]]);
        }
    }
}
