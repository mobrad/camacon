<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'telephone' => $faker->phoneNumber,
        'password' => bcrypt('tommy'),
        'address' => $faker->address,
        'ip_address' => $faker->ipv4,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\PortfolioImage::class, function (Faker $faker) {
    return [
        'photography_category_id' => '',
        'photographer_id' => 1,
        'name' => 'images/'.$faker->firstName,
    ];
});

$factory->define(App\Photographer::class, function (Faker $faker) {
    $email = $faker->unique()->safeEmail;

    return [
            'full_name' => $faker->firstName.' '.$faker->lastName,
            'studio_name' => $faker->firstName.' Studios',
            'tel' => 2348118022308,
            'email' => $faker->email,
            'city' => 'Port Harcourt',
            'address' => 'Plot 257 Eagle Island',
            'password' => bcrypt('tommy'),
            'shoot_hour' => 3,
            'confirmation_token' => str_limit(md5($email.str_random()), 25, ''),
    ];
});

$factory->define(App\PhotographerCamera::class, function (Faker $faker) {
    return [
        'photographer_id' => 1,
        'camera_id' => 1,
    ];
});

$factory->define(App\PhotographerLens::class, function (Faker $faker) {
    return [
        'photographer_id' => 1,
        'lens_id' => 1,
    ];
});

$factory->define(App\Camera::class, function (Faker $faker) {
    return [
        'name' => 'Canon 50 X',
    ];
});

$factory->define(App\Lens::class, function (Faker $faker) {
    return [
        'name' => 'Focal 4',
    ];
});

$factory->define(App\PhotographyCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'image' => 'avatar/avatar.png'
    ];
});

$factory->define(App\Package::class, function (Faker $faker) {
    return [
        'plan_name' => $faker->word(),
        'plan_price' => rand(2000, 20000),
        'plan_description' => $faker->sentence(),
    ];
});

$factory->define(App\Experience::class, function (Faker $faker) {
    return [
           'photographer_id' => 1,
           'years_of_experience' => 4,
           'describe_experience' => 'This is how i can describe my experience by letting you know i have no experience',
           'photography_passion' => 'This is how i can describe my experience by letting you know i have no experience',
           'begin_photography' => 'This is how i can describe my experience by letting you know i have no experience',
           'love_your_job' => 'This is how i can describe my experience by letting you know i have no experience',
           'your_shoot_type' => 'This is how i can describe my experience by letting you know i have no experience',
           'type_of_shoot_done' => 'This is how i can describe my experience by letting you know i have no experience',
           'favorite_place_for_shoot' => 'This is how i can describe my experience by letting you know i have no experience',
           'something_about_you' => 'This is how i can describe my experience by letting you know i have no experience',
    ];
});

$factory->define(App\Portfolio::class, function (Faker $faker) {
    return [
        'photographer_id' => 1,
        'portfolio_link' => 'facebook.com',
        'instagram_handle' => 'facebook.com',
        'facebook_page' => 'facebook.com',
        'twitter_handle' => 'facebook.com',
    ];
});
