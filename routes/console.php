<?php

use App\User;
use App\Photographer;
use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('randomUserLogin', function () {
    $user = User::all()->take(5)->random();
    $this->comment($user->email);
})->describe('Displays a user email');

Artisan::command('photographer_login', function () {
    $user = Photographer::all()->take(5)->random();
    $this->comment($user->email);
})->describe('Displays a photographers email or login');
