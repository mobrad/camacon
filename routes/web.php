<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/photographer/login', 'Auth\PhotographerLoginController@login')
    ->name('photographer.login');
Route::post('/photographer/register/experience', 'ExperienceController@store')
    ->name('photographer.register.experience');
Route::post('/photographer/register/experience/{id}', 'ExperienceController@update')
    ->name('photographer.register.experience.update');
Route::post('/photographer/register/equipment', 'EquipmentController@store')
    ->name('photographer.register.equipment');

Route::post('/photographer/register/portfolio', 'PortfolioController@store')
    ->name('photographer.register.portfolio');
Route::get('/photographer/register', 'PhotographerController@create')->name('photographer.register');
Route::post('/photographer/register', 'PhotographerController@store')->name('photographer');
Route::post('/photographer/register/{id}', 'PhotographerController@update')->name('photographer.update ');
Route::get('/photographer/register/complete', 'PhotographerController@complete')->name('complete');
Route::get('/photographer/register/confirm', 'Auth\RegistrationConfirmationController@photographer')
    ->name('photographer.register.confirm');
Route::get('/user/register/confirm', 'Auth\RegistrationConfirmationController@user')->name('user.register.confirm');

Route::get('/how-it-works', function () {
    return view('how-it-works');
})->name('how-it-works');
Route::get('category/{category}','ImageCategoryController@show')->name('category');
//guest routes
Route::group(['prefix' => 'photographers'], function () {
    Route::get('/', 'PhotographerController@index')->name('photographer-finder');
    Route::get('/{id}', 'PhotographerPortfolioController@show')->where('id', '\d+')->name('portfolio');

    //logged in user routes
    Route::group(['middleware' => 'auth:web'], function () {
        Route::post('/likes/{image_id}', 'LikeController@store')->name('images.like');
        Route::get('/{id}/booking', 'BookingController@show')->name('booking')->middleware('auth:web');
    });
});

//logged in photographer routes
Route::group(['middleware' => 'auth:photographer'], function () {
    Route::get('/dashboard', 'PhotographerPortfolioController@show')->name('photographer.portfolio');
});

Route::post('/photographer/booking', 'BookingController@store')->name('book.photographer');

Auth::routes();
