<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cameras', function () {
    return json_encode(\App\Camera::all()->toArray());
});

Route::resource('/lenses', 'LensController')->only([
    'index', 'show',
]);

Route::post('/special-cameras', function () {
    \App\SpecialCamera::forceCreate([
        'name' => request('name'),
    ]);
});

Route::group(['prefix' => '/photographers/{photographer}/'], function () {
    Route::get('rating', 'RatingController@show')->name('rating');
    Route::post('rating', 'RatingController@store')->name('rating.store')->middleware('auth:api');

    Route::get('images', 'PhotographerImageController@getImages')->where('photographer', '\d+')->name('photographer.images');
    Route::get('equipments', 'PhotographerController@getEquipments')->where('photographer', '\d+')->name('photographer.equipments');
});

Route::post('/photographer/image', 'PhotographerImageController@store')->name('photographer.image');
Route::post('/photographer/avatar', 'PhotographerAvatarController@store')->name('photographer.avatar');

Route::group(['prefix' => '/images/{image_id}'], function () {
    Route::post('like', 'LikeController@store')->name('image.like')->middleware('auth:api');
    Route::get('likes', 'PhotographerImageController@getImageLikes')
        ->where('image_id', '\d+')->name('photographer.images');
    Route::get('likes/{user_id}', 'PhotographerImageController@getImageLikes')
        ->where(['image_id' => '\d+', 'user_id' => '\d+']);
});
