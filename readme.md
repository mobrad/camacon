<div>
<h1 style="color:red;text-align:center;">Spectra</h1>
<p>An uber for photography in Nigeria</p>
</div>
<div>
<p>Spectra makes booking photographers in Nigeria as Easy as Possible and also gives you the best photographer for the best prices</p>

<h3>Features of Spectra </h3>
<ul>
    <li>View Photographers portfolio and Equipments</li>
    <li>Allow photographers to be rated by Users</li>
    <li>Book photographer close to your location at cheap rate </li>
    <li>Users have choice of viewing their prefered photographer</li>
</ul>
</div>
<div>

<h3>Frameworks used </h3>
<ul>
    <li>Laravel</li>
    <li>Jquery</li>
    <li>VueJs</li>
    <li>Bootstraps</li>
</ul>
</div>
<div>

<h3>Vuejs Libraries Used </h3>
<ul>
    <li>Form wizard</li>
    <li>Vuelidate</li>
    <li>Vue Multi Select</li>
    <li>Types.js</li>
    <li>Vue Carousel</li>
</ul>
</div>