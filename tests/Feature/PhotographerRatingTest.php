<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PhotographerRatingTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function setup(){
        parent::setUp();
        $this->photographer = create('App\Photographer');
    }
    /** @test */
   public function an_unauthenticated_user_can_not_rate_a_photographer(){
       //Send a post request that is not from an authenticated user
       $this->withExceptionHandling()
           ->post('/api/photographer/'.$this->photographer->id.'/rating')
           ->assertStatus(500);
       //assert that he gets a an unauthenticated response
   }
   /** @test */
   public function authenticated_user_can_rate_a_photographer(){
       $this->signIn();
       $this->post('/api/photographer/'.$this->photographer->id.'/rating',[
           'user_id' => auth()->user()->id,
           'photographer_id' => $this->photographer->id,
           'rating' => 5
       ])
       ->assertStatus(200);
   }
    /** @test */
    public function an_authenticated_user_can_update_a_photographer_rating(){
        $this->signIn();
        $this->post('/api/photographer/'.$this->photographer->id.'/rating',[
            'user_id' => auth()->user()->id,
            'photographer_id' => $this->photographer->id,
            'rating' => 5
        ]);
        $this->post('/api/photographer/'.$this->photographer->id.'/rating',[
            'user_id' => auth()->user()->id,
            'photographer_id' => $this->photographer->id,
            'rating' => 3
        ])->assertStatus(200);
    }

}
