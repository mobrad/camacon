<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function a_user_can_view_photos_by_their_category(){
        $category = create('App\PhotographyCategory');
        $photographer = create('App\Photographer');
        $image = create('App\PortfolioImage',['photography_category_id' => $category->id,'photographer_id' => $photographer->id]);
        $this->get(route('category',$category))->assertSee($image->name);

    }
}
