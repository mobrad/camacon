<?php

namespace Tests\Feature;

use App\Events\PhotographerRegistered;
use App\Mail\PleaseConfirmYourEmail;
use App\Photographer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;




class PhotographerRegistrationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @param array $override
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function createPhotographer($override =[])
    {
       
    }
    /** @test */
    public function a_guest_can_apply_to_be_a_photographer(){
       $this->withExceptionHandling();
        $photographer = create('App\Photographer');
        $this->post(route('photographer'),$photographer->toArray());
        $this->assertDatabaseHas('photographers',['full_name' =>'Bradley Yarrow','tel' => 2348118022308]);
    }
    /** @test */
    public function a_guest_who_applied_must_first_confirm_their_email_address(){
        //A user will first register
        // $this->withExceptionHandling();
         $this->withExceptionHandling();
        $photographer = create('App\Photographer');
        $this->post(route('photographer'),$photographer->toArray());
      
        $photographer = Photographer::whereFullName('Bradley Yarrow')->first();
        $this->assertFalse($photographer->confirmed);
        //A token will be generated for the user
        $this->assertNotNull($photographer->confirmation_token);
        //An email will be sent to the Photographer that just registered
       
    }

    /** @test */
    public function an_email_is_sent_on_photographers_registration(){
        Mail::fake();
        event(new PhotographerRegistered(create('App\Photographer')));
        Mail::assertSent(PleaseConfirmYourEmail::class);
    }
    /** @test */
    public function a_phoptographer_can_register_his_equipments(){
        //Create the photographer
        $this->withExceptionHandling();
        $photographer = create('App\Photographer');
        $this->post(route('photographer'),$photographer->toArray());
        $photographer = Photographer::whereFullName('Bradley Yarrow')->first();
        $camera_value = create('App\Camera');
          foreach ($camera_value as $value){
            $equipment = create('App\PhotographerCamera',[
                'photographer_id' => $photographer->id,
                'camera_id' => $value
            ]);
        }
        $lens_value = create('App\Lens');
        foreach ($lens_value as $value){
            $equipment = create('App\PhotographerLens',[
                'photographer_id' => $photographer->id,
                'lens_id' => $value
            ]);
        }
        $this->assertDatabaseHas('photographer_camera',['photographer_id' =>1]);
        $this->assertDatabaseHas('photographer_lens',['photographer_id' =>1]);
    }
     /** @test */
     public function a_photographer_can_add_experience(){
        $this->withExceptionHandling();
        $photographer = create('App\Photographer');
        $this->post(route('photographer'),$photographer->toArray());
        $photographer = Photographer::whereFullName('Bradley Yarrow')->first();
        $experience = create('App\Experience',[
            'photographer_id' => $photographer->id
        ]);
        $this->post(route('photographer.register.experience'),$experience->toArray());
        $this->assertDatabaseHas('experiences',['photographer_id' =>  $photographer->id]);
     }

     /** @test */
     public function a_photographer_can_add_portfolio(){
        $this->withExceptionHandling();
        $photographer = create('App\Photographer');
        $this->post(route('photographer'),$photographer->toArray());
        $photographer = Photographer::whereFullName('Bradley Yarrow')->first();
        $portfolio = create('App\Portfolio',[
            'photographer_id' => $photographer->id
        ]);
        $this->post(route('photographer.register.portfolio'),$portfolio->toArray());
        $this->assertDatabaseHas('portfolios',['photographer_id' =>  $photographer->id]);
     }
}
