<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * @property mixed photographer
 */
class PhotographerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function setup(){
        parent::setUp();
        $this->photographer = create('App\Photographer');
    }
    /** @test */
   public function a_guest_can_view_available_photographers(){
        $this->get('/photographers')
            ->assertSee($this->photographer->studio_name);
   }
    /** @test */
   public function a_photographer_has_a_path(){
       $this->assertEquals( '/photographers/'.$this->photographer->id,$this->photographer->path());
   }
    /** @test */
    public function a_user_can_view_a_single_photographer(){
        $this->get($this->photographer->path())->assertSee($this->photographer->full_name);
    }
    /** @test */
    public function a_photographer_can_update_his_equipments(){

    }
    /** @test */
    public function a_photographer_can_add_more_images(){

    }

}
